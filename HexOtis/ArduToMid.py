def creation_matrice(nombretemps=16 , decoupagetemps=4 , nombreNote=72):
    #Creation de la matrice nombre de note*nombre de temps sous la forme [          ][          ]...
    return nombreNote*('[' + nombretemps*('.' + (decoupagetemps - 1)*' ') + ']') + ';'
    
#changement du repertoire de travail courant...
import os
os.chdir("F:/HexOtis")
chaine1 = open("argTtM1(NEPASMODIFIER).txt" , 'r')
Chaine1 = chaine1.read()
chaine1.close()
chaine2 = open("argTtM2(NEPASMODIFIER).txt" , 'r')
Chaine2 = chaine2.read()
chaine2.close()


#lecture du fichier texte demandé...
Filenotfound = True
while Filenotfound:
    try:
        Fichieràtraiter = input ("Fichier à convertir?")
        Txt = '.txt'
        Fichieràtraitertxt = Fichieràtraiter + Txt
        fichierHexotis = open(Fichieràtraitertxt , 'r')
        TxtArduino = fichierHexotis.read()
        fichierHexotis.close()
        Filenotfound = False
    except FileNotFoundError:   
        print("Le fichier", Fichieràtraitertxt , "n'existe pas ou ne se trouve pas dans le repertoire de travail courant")
        
#lecture du txt reçu et conversion...
Chainemodifiée = creation_matrice(ord(TxtArduino[:1]) - 32 , ord(TxtArduino[1:2]) - 32 , ord(TxtArduino[2:3]) - 32)
k = 0
for lettre in TxtArduino[3:]:
    if k%3 == 0:
        if lettre == ' ':
            break
        Note = ord(lettre) - 31
    elif k%3 == 1:
        Temps = ord(lettre) - 32
    else:
        if ord(lettre) == 32:
            l = 0
            while (Chainemodifiée[-66 * Note + Temps - l] == ' ') or (Chainemodifiée[-66 * Note + Temps -l] == '.'):
                Chainemodifiée = Chainemodifiée[ :-66 * Note + Temps - l] + '-' + Chainemodifiée[-66 * Note + Temps - l + 1: ]
                l = l + 1
        else:
            Chainemodifiée = Chainemodifiée[ :-66 * Note + Temps] + lettre + Chainemodifiée[-66 * Note + Temps + 1: ]
    k += 1

#creation du MIDI via TxtToMidi...

Conversionfinale = Chaine1 + Chainemodifiée + Chaine2
fichierHexotis = open("argTtM3(NEPASMODIFIER).txt" , 'w')
fichierHexotis.write(Conversionfinale)
CommandeTextToMidi = "F:\\HexOtis\\textToMidi.exe F:\\HexOtis\\argTtM3(NEPASMODIFIER).txt F:\\HexOtis\\" + Fichieràtraiter + ".mid"
fichierHexotis.close()
os.system(CommandeTextToMidi)

#fichierHexotis = open(Fichieràtraitertxt , 'w')
#fichierHexotis.write(TxtArduino)
#fichierHexotis.close()
